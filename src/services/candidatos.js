
import { http } from './config'

export default{
    listar:() => {
        return http.get('candidatos')
    },
    salvar:(candidato) => {
        return http.post('candidatos', candidato)
    },
    atualizar:(candidato) => {
        return http.put(`candidatos/${candidato.id}`, candidato)
    },
    apagar:(candidato) => {
        console.log(candidato)
        return http.delete(`candidatos/${candidato.id}`)
    },
}