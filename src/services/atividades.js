
import { http } from './config'

export default{
    listar:() => {
        return http.get('atividades')
    },
    salvar:(atividade) => {
        return http.post('atividades', atividade)
    },
    atualizar:(atividade) => {
        return http.put(`atividades/${atividade.id}`, atividade)
    },
    apagar:(atividade) => {
        console.log(atividade)
        return http.delete(`atividades/${atividade.id}`)
    },
}