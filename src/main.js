import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'popper.js/dist/popper.min.js'
import 'jquery/dist/jquery.min.js'

import Candidato from './components/Candidato.vue'
// import Principal from './components/Principal.vue'
import Img from './components/imgSecif.vue'
import Atividade from './components/Atividade.vue'
import Evento from './components/Evento.vue'


require('bootstrap')

Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [
  {path: '/eventos', component: Evento},
  {path: '/atividades', component: Atividade},
  {path: '/candidatos', component: Candidato},
  {path: '/', component: Img},
]

const router = new VueRouter({
  routes: routes
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
